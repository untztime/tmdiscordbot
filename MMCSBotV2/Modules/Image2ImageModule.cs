﻿using Discord;
using Discord.Interactions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MMCSBotV2.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static MMCSBotV2.Modules.InteractionModule;

namespace MMCSBotV2.Modules;

public class Image2ImageModule : InteractionModuleBase<SocketInteractionContext>
{
    private readonly IServiceProvider _provider;
    private readonly IConfiguration _config;
    public Image2ImageModule(IServiceProvider provider)
    {
        _provider = provider;
        _config = provider.GetRequiredService<IConfigurationRoot>();
    }



    [RequireNsfw]
    [SlashCommand("img2img", "Image to image generator")]
    public async Task HandleImg2ImgModalInput
        (
            string url,
            string? prompt,
            string? negativePrompt = "",
            Orientation orientation = Orientation.Square,
            int cfgScale = 7,
            BatchSize batchSize = BatchSize.One,
            EnhanceType autoEnhance = EnhanceType.None,
            bool privatePrompt = false,
            int seed = -1
        )
    {
        if ((!url.StartsWith("https://cdn.discordapp.com") && !url.StartsWith("https://media.discordapp.com")) && (!url.Contains(".png") || !url.Contains(".jpg") || !url.Contains(".jpeg")))
        {
            await FollowupAsync("Please only use discord png or jpg links");
            return;
        }

        Tools.Tools tools = new();

        url = tools.SanitizeUrl(url);

        await DeferAsync(ephemeral: privatePrompt);

        int height = 512; int width = 512; // default

        int bsize = 1;

        // limit batchsize if landscape for vram reasons
        if (batchSize == BatchSize.Five && orientation == Orientation.Landscape)
            batchSize = BatchSize.Four;

        switch (batchSize)
        {
            case BatchSize.One:
                bsize = 1; break;
            case BatchSize.Two:
                bsize = 2; break;
            case BatchSize.Three:
                bsize = 3; break;
            case BatchSize.Four:
                bsize = 4; break;
            case BatchSize.Five:
                bsize = 5; break;
        }

        switch (orientation)
        {
            case Orientation.Landscape:
                height = 576; width = 960; break; //height = 512; width = 960; break;

            case Orientation.Portrait:
                height = 960; width = 512; break;

            case Orientation.Square:
                height = 512; width = 512; break;
        }

        if (prompt == null)
            return;

        PromptEnhance enhancedRequest = new PromptEnhance(_provider)
        {
            InitialPrompt = prompt,
            InitialNegativePrompt = negativePrompt,
            Type = autoEnhance,
        };
        enhancedRequest.GenerateEnhancedPrompt();
        prompt = enhancedRequest.NewPrompt;
        negativePrompt = enhancedRequest.NewNegativePrompt;

        Uri.TryCreate(url, UriKind.Absolute, out var uri);
        var ext = Path.GetExtension(uri.LocalPath);

        var guid = Guid.NewGuid().ToString();

        var base64 = tools.ConvertUrlImageToBase64StringAsync(url).Result;

        var imagePromptReq = new Img2Img(_provider)
        {
            Prompt = prompt,
            Width = width,
            Height = height,
            CFG = cfgScale,
            Base64EncodedImage = base64,
            ImageType = ext,
            Seed = seed,
        };

        if (negativePrompt != null)
            imagePromptReq.NegativePrompt = negativePrompt;

        imagePromptReq.SetJsonRequestObject();
        await imagePromptReq.PostImg2ImgRequest();
        var attachments = imagePromptReq.GetFileAttachments();

        seed = imagePromptReq.GetSeed();

        string seedMsg = "";

        if (bsize == 1)
            seedMsg = seedMsg + $"Seed: {seed}";
        if (bsize > 1)
        {
            seedMsg = "Seeds: ";
            for (int i = 0; i < bsize; i++)
            {
                seedMsg = seedMsg + $"{i}: {seed + i}  ";
            }
        }

        var glitterMaker = new MessageGlitter();
        string glitter = glitterMaker.GenerateGlitter(prompt);

        if (!privatePrompt)
        {
            var builder = new ComponentBuilder()
                .WithButton("Delete", "deleteimage", ButtonStyle.Secondary);
            var messageComponent = builder.Build();
            await FollowupWithFilesAsync(attachments, components: messageComponent, ephemeral: privatePrompt, text: seedMsg + " " + glitter + $"\nOriginal URL: {url}\nPrompt: {prompt}\nNegative:{negativePrompt}");
        }
        if (privatePrompt)
            await FollowupWithFilesAsync(attachments, ephemeral: privatePrompt, text: seedMsg + " " + glitter + $"\nPrompt: {prompt}\nNegative:{negativePrompt}");

    }

}


internal class Img2Img
{
    private readonly IConfiguration _config;
    public Img2Img(IServiceProvider provider)
    {
        _config = provider.GetRequiredService<IConfigurationRoot>();
    }

    public string Prompt { get; set; } = string.Empty;
    public string NegativePrompt { get; set; } = string.Empty;
    public int CFG { get; set; } = 7;
    public float DenoiseStrength { get; set; } = 0.75f;
    public int Height { get; set; } = 512;
    public int Width { get; set; } = 512;
    public string Base64EncodedImage { get; set; } = string.Empty;
    public string ImageType { get; set; } = ".png";
    public Guid Guid { get; set; } = Guid.NewGuid();
    public int BatchSize { get; set; } = 1;
    public int Seed { get; set; } = -1;

    public readonly string SampleImg2ImgJsonRequest = System.IO.File.ReadAllText("samplejson-img2img.txt");

    private string? NewJson = "";

    public List<FileAttachment> attachments = new List<FileAttachment>();

    public List<FileAttachment> GetFileAttachments()
    {
        return attachments;
    }

    public int GetSeed()
    {
        return Seed;
    }

    public void SetJsonRequestObject()
    {
        if (Seed == -1)
        {
            var rnd = new Random(DateTime.Now.Millisecond);
            Seed = rnd.Next(10000, 999999999);
        }

        var newJson = JsonNode.Parse(SampleImg2ImgJsonRequest);

        Console.WriteLine($"Checking extension: {ImageType}");
        if (ImageType == ".png")
        {
            Console.WriteLine("Inserting b64 json data for png");
            Base64EncodedImage = Base64EncodedImage.Insert(0, "data:image/png;base64,");
        }

        if (ImageType == ".jpg" || ImageType == ".jpeg")
        {
            Console.WriteLine("Inserting b64 json data for jpg");
            Base64EncodedImage = Base64EncodedImage.Insert(0, "data:image/jpeg;base64,");
        }

        if (newJson != null)
        {
            newJson["data"][1] = Prompt;
            newJson["data"][2] = NegativePrompt;
            newJson["data"][5] = Base64EncodedImage;
            newJson["data"][10] = 40; // steps
            newJson["data"][19] = DenoiseStrength; //denoise strength
            newJson["data"][20] = Seed;
            newJson["data"][26] = Height;
            newJson["data"][27] = Width;
        }

        NewJson = newJson?.ToJsonString();

        Console.WriteLine(NewJson);

        NewJson = JsonSerializer.Serialize(newJson);
    }

    public string? GetJsonRequestObjectString()
    {
        return NewJson;
    }

    public async Task PostImg2ImgRequest()
    {
        Tools.Tools tools = new();
        string? jsonImgToImageRequestString = NewJson;

        string responseString = "";

        string host = _config["SDHostBasePrimary"];
        if (!tools.CheckHttpHostOnline(_config["SDHostBasePrimary"]).Result)
            host = _config["SDHostBaseSecondary"];

        using (var httpClient = new HttpClient { BaseAddress = new Uri(host) })
        {
            httpClient.Timeout = TimeSpan.FromMinutes(10);
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue("MMCSBot", "1.0"));

            try
            {
                var requestContent = new StringContent(jsonImgToImageRequestString, Encoding.UTF8, "application/json");

                var result = await httpClient.PostAsync("api/predict", requestContent);

                responseString = await result.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Request failed. {ex.Message}");
            }

        }

        var jsonResponse = JsonNode.Parse(responseString);

        for (int i = 0; i < BatchSize; i++)
        {
            string base64ImageText = "";
            try
            {
                if (BatchSize == 1)
                    base64ImageText = jsonResponse?["data"]?[0][0]?.ToString();


                if (BatchSize > 1)
                {
                    base64ImageText = jsonResponse?["data"]?[0][i + 1]?.ToString();
                }
            }
            catch (Exception ex) { Console.WriteLine($"Warning: Probably ran out of VRAM. {ex}"); }


            if (base64ImageText != null)
            {
                //clean up browser/json base64
                base64ImageText = Regex.Unescape(base64ImageText);
                base64ImageText = base64ImageText?
                    .Remove(0, 22)
                    .Trim();
            }

            Stream stream = new MemoryStream(Convert.FromBase64String(base64ImageText));

            attachments?.Add(new FileAttachment(stream, $"{Guid.ToString()}-{i}.png", Prompt, false));
        }
    }
}
