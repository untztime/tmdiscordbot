﻿using Discord.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMCSBotV2.Modules;

public class DeleteButtonModule : InteractionModuleBase<SocketInteractionContext>
{
    [ComponentInteraction("deleteimage")]
    public async Task DeleteButtonHandler()
    {
        await DeferAsync();

        var originalResponse = await GetOriginalResponseAsync();
        Console.WriteLine(originalResponse.CreatedAt.ToString());

        int maxMinutessBeforeCanDelete = 3;
        var timeLeft = originalResponse.CreatedAt.AddMinutes(maxMinutessBeforeCanDelete) - DateTime.Now;

        Console.WriteLine($"{DateTime.Now.ToString()}: Attempted deletion of {originalResponse.CreatedAt.ToString()} author: {originalResponse.Author.Username} by: {Context.User.Username}.");

        Console.WriteLine(timeLeft.TotalSeconds.ToString());

        if (0 > timeLeft.TotalSeconds || Context.User.Username == "Teaman")
        {
            await DeleteOriginalResponseAsync();
            await FollowupAsync($"{Context.User.Mention} has deleted an image.");
            Console.WriteLine("Image deleted.");
        }
        else
        {
            await FollowupAsync($"{Context.User.Mention} you're trying to delete an image too quickly. Time left: {Math.Round(timeLeft.TotalSeconds, MidpointRounding.ToEven)} seconds", ephemeral: true);
        }
    }
}
