﻿using Discord;
using Discord.Interactions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Net;
using System.Text;
using System.Text.Json.Nodes;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static MMCSBotV2.Modules.InteractionModule;
using System.Text.Json;
using System.IO;
using Discord.Commands;

namespace MMCSBotV2.Modules;

public class UpscaleModule : InteractionModuleBase<SocketInteractionContext>
{
    private readonly IServiceProvider _provider;
    private readonly IConfiguration _config;
    public UpscaleModule(IServiceProvider provider)
    {
        _provider = provider;
        _config = provider.GetRequiredService<IConfigurationRoot>();
    }

    [SlashCommand("upscale", "Upscale an image 4x")]
    public async Task HandleUpscaleCommand(string url, bool privatePrompt = false, double factor = 2.0f)
    {
        Tools.Tools tools = new();

        url = tools.SanitizeUrl(url);

        // many models dont support more than 4x upscale
        if (factor > 4)
            factor = 4;

        await DeferAsync(ephemeral: privatePrompt);

        if ((!url.StartsWith("https://cdn.discordapp.com") && !url.StartsWith("https://media.discordapp.net")) && (!url.Contains(".png") || !url.Contains(".jpg") || !url.Contains(".jpeg")))
        {
            await FollowupAsync("Please only use discord png or jpg links");
            return;
        }

        if (url != null)
        {
            var upscaleRequest = new Upscale(_config)
            {
                URL = url,
                Factor = factor
            };

            await upscaleRequest.SetJsonRequestObject();
            await upscaleRequest.PostUpscaleRequest();
            var attachments = upscaleRequest.GetFileAttachments();

            Console.WriteLine($"{url}");

            if (!privatePrompt)
            {
                var builder = new ComponentBuilder()
                    .WithButton("Delete", "deleteimage", ButtonStyle.Secondary);
                var messageComponent = builder.Build();
                await FollowupWithFilesAsync(attachments, components: messageComponent, ephemeral: privatePrompt);
            }
            if (privatePrompt)
                await FollowupWithFilesAsync(attachments, ephemeral: privatePrompt);
        }

    }
}


internal class Upscale
{
    public Upscale(IConfiguration config)
    {
        _config = config;
    }
    private readonly IConfiguration _config;

    public string URL { get; set; } = string.Empty;
    public double Factor { get; set; } = 2;
    public Guid Guid { get; set; } = Guid.NewGuid();
    public string ImageType { get; set; } = ".png";
    public readonly string SampleJsonRequest = System.IO.File.ReadAllText("samplejson-upscale.txt");
    private string? NewJson = "";

    public string Base64EncodedImage { get; set; } = string.Empty;

    public List<FileAttachment>? attachments = new List<FileAttachment>();

    public async Task SetJsonRequestObject()
    {

        var newJson = JsonNode.Parse(SampleJsonRequest);

        await ConvertUrlImageToBase64StringAsync(URL);

        Console.WriteLine($"Checking extension: {ImageType}");
        if (ImageType == ".png")
        {
            Base64EncodedImage = Base64EncodedImage.Insert(0, "data:image/png;base64,");
        }

        if (ImageType == ".jpg" || ImageType == ".jpeg")
        {
            Base64EncodedImage = Base64EncodedImage.Insert(0, "data:image/jpeg;base64,");
        }


        if (newJson != null)
        {
            newJson["data"][1] = Base64EncodedImage;
            newJson["data"][6] = Factor; // upscale x
        }

        NewJson = newJson?.ToJsonString();

        Console.WriteLine(NewJson);

        NewJson = JsonSerializer.Serialize(newJson);
    }

    public async Task ConvertUrlImageToBase64StringAsync(string url)
    {
        byte[] imageByteArray = null;
        using (var client = new HttpClient())
        {
            var response = client.GetAsync(url).GetAwaiter().GetResult();
            if (response.IsSuccessStatusCode)
            {
                imageByteArray = await response.Content.ReadAsByteArrayAsync();
            }
        }
        
        var imageBase64 = Convert.ToBase64String(imageByteArray);

        Base64EncodedImage = imageBase64;
    }

    public async Task PostUpscaleRequest()
    {
        Tools.Tools tools = new();
        string? jsonImgToImageRequestString = NewJson;

        string responseString = "";

        string host = _config["SDHostBasePrimary"];
        if (!tools.CheckHttpHostOnline(_config["SDHostBasePrimary"]).Result)
            host = _config["SDHostBaseSecondary"];

        using (var httpClient = new HttpClient { BaseAddress = new Uri(host) })
        {
            httpClient.Timeout = TimeSpan.FromMinutes(10);
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue("MMCSBot", "1.0"));

            try
            {
                var requestContent = new StringContent(jsonImgToImageRequestString, Encoding.UTF8, "application/json");
                var result = await httpClient.PostAsync("api/predict", requestContent);
                responseString = await result.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Request failed. {ex.Message}");
            }

        }

        var jsonResponse = JsonNode.Parse(responseString);

        string? base64ImageText = jsonResponse?["data"]?[0][0]?.ToString();

        if (base64ImageText != null)
        {
            base64ImageText = Regex.Unescape(base64ImageText);
            base64ImageText = base64ImageText?
                .Remove(0, 22)
                .Trim();
        }

        var imageBytes = Convert.FromBase64String(base64ImageText);
        Stream stream = new MemoryStream(imageBytes);

        attachments.Add(new FileAttachment(stream, $"{Guid.ToString()}.png", "Upscaled.", false));
    }

    public List<FileAttachment> GetFileAttachments()
    {
        return attachments;
    }
}