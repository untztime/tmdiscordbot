﻿using Discord;
using Discord.Interactions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MMCSBotV2.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static MMCSBotV2.Modules.InteractionModule;

namespace MMCSBotV2.Modules;

public class Text2Image : InteractionModuleBase<SocketInteractionContext>
{
    private readonly IServiceProvider _provider;
    private readonly IConfiguration _config;
    public Text2Image(IServiceProvider provider)
    {
        _provider = provider;
        _config = provider.GetRequiredService<IConfigurationRoot>();
    }


    [RequireNsfw]
    [SlashCommand("txt2img", "Text to image generator")]
    public async Task HandleTextToImageCommand(string? prompt,
            string? negativePrompt = "",
            Orientation orientation = Orientation.Square,
            int cfgScale = 7,
            BatchSize batchSize = BatchSize.One,
            EnhanceType autoEnhance = EnhanceType.None,
            bool privatePrompt = false,
            int seed = -1,
            bool highRes = false
        )
    {
        Tools.Tools tools = new();

        if (Context.Channel.Id == 355897045703458819 || Context.Channel.Id == 882744170320650264)
        {
            await DeferAsync(ephemeral: true);
            await FollowupAsync("You can't use this channel.", ephemeral: true);
            return;
        }

        int height = 640; int width = 640; // default
        bool censorImage = false;

        int bsize = 1;

        switch (batchSize)
        {
            case BatchSize.One:
                bsize = 1; break;
            case BatchSize.Two:
                bsize = 2; break;
            case BatchSize.Three:
                bsize = 3; break;
            case BatchSize.Four:
                bsize = 4; break;
            case BatchSize.Five:
                bsize = 5; break;
        }

        switch (orientation)
        {
            case Orientation.Landscape:
                height = 512; width = 768; break;
            case Orientation.Portrait:
                height = 768; width = 512; break;
            case Orientation.Square:
                height = 640; width = 640; break;
        }

        if (highRes)
        {
            if (bsize > 4)
                bsize = 4;

            height = height * 2;
            width = width * 2;
        }

        Console.WriteLine($"{DateTime.Now.ToString()}: Request received: \"{prompt}\" by: {Context.User.Username}. Channel: {Context.Channel.Name}");

        string[] bannedWords = { "testbannedword" };
        string[] warningWords = { "testwarningword" };

        if (prompt == null)
            return;

        foreach (var bannedWord in bannedWords)
        {
            if (prompt.Contains(bannedWord))
                prompt = prompt.Replace(bannedWord, "");
        }

        foreach (var warningWord in warningWords)
        {
            if (prompt.Contains(warningWord))
                censorImage = false;
        }

        if (censorImage)
        {
            if (!prompt.Contains("anime"))
                prompt = prompt + " anime";

            negativePrompt = negativePrompt + " photo, realistic, canon50";
            privatePrompt = true;
        }
        
        //prompt = tools.CleanInput(prompt);

        PromptEnhance enhancedRequest = new PromptEnhance(_provider)
        {
            InitialPrompt = prompt,
            InitialNegativePrompt = negativePrompt,
            Type = autoEnhance,
        };
        enhancedRequest.GenerateEnhancedPrompt();
        prompt = enhancedRequest.NewPrompt;
        negativePrompt = enhancedRequest.NewNegativePrompt;

        await DeferAsync(ephemeral: privatePrompt);


        var imagePromptReq = new Txt2Img(_provider)
        {
            Prompt = prompt,
            Width = width,
            Height = height,
            CFG = cfgScale,
            Seed = seed,
            BatchSize = bsize,
            NegativePrompt = negativePrompt,
            HighRes = highRes,
        };

        imagePromptReq.SetJsonRequestObject();
        await imagePromptReq.PostTxt2ImgRequest();
        var attachments = imagePromptReq.GetFileAttachments();

        seed = imagePromptReq.GetSeed();

        string seedMsg = "";

        if (bsize == 1)
            seedMsg = seedMsg + $"Seed: {seed}";
        if (bsize > 1)
        {
            seedMsg = "Seeds: ";
            for (int i = 0; i < bsize; i++)
            {
                seedMsg = seedMsg + $"{i}: {seed + i}  ";
            }
        }

        var glitterMaker = new MessageGlitter();
        string glitter = glitterMaker.GenerateGlitter(prompt);

        if (!privatePrompt)
        {
            var builder = new ComponentBuilder()
                .WithButton("Delete", "deleteimage", ButtonStyle.Secondary);
            var messageComponent = builder.Build();
            await FollowupWithFilesAsync(attachments, components: messageComponent, ephemeral: privatePrompt, text: seedMsg + " " + glitter + $"\nPrompt: {prompt}\nNegative:{negativePrompt}");
        }
        if (privatePrompt)
            await FollowupWithFilesAsync(attachments, ephemeral: privatePrompt, text: seedMsg + " " + glitter + $"\nPrompt: {prompt}\nNegative:{negativePrompt}");

    }
}


internal class Txt2Img
{

    private readonly IConfiguration _config;
    public Txt2Img(IServiceProvider provider)
    {
        _config = provider.GetRequiredService<IConfigurationRoot>();
    }

    public string Prompt { get; set; } = string.Empty;
    public string NegativePrompt { get; set; } = string.Empty;
    public int CFG { get; set; } = 7;
    public int Height { get; set; } = 512;
    public int Width { get; set; } = 512;
    public int Seed { get; set; } = -1;
    public Guid Guid { get; set; } = Guid.NewGuid();
    public int BatchSize { get; set; } = 1;

    public bool HighRes { get; set; } = false;

    public readonly string SampleJsonRequest = System.IO.File.ReadAllText("samplejson-txt2img.txt");
    private string? NewJson = "";

    public List<FileAttachment>? attachments = new List<FileAttachment>();

    public void SetJsonRequestObject()
    {
        if (Seed == -1)
        {
            var rnd = new Random(DateTime.Now.Millisecond);
            Seed = rnd.Next(10000, 999999999);
        }

        var newJson = JsonNode.Parse(SampleJsonRequest);

        if (newJson != null)
        {
            newJson["data"][0] = Prompt;
            newJson["data"][1] = NegativePrompt;
            newJson["data"][4] = 42; //steps
            newJson["data"][9] = BatchSize;
            newJson["data"][10] = CFG;
            newJson["data"][11] = Seed;
            newJson["data"][17] = Height;
            newJson["data"][18] = Width;
            newJson["data"][19] = HighRes;
        }

        NewJson = newJson?.ToJsonString();

        Console.WriteLine(NewJson);

        NewJson = JsonSerializer.Serialize(newJson);
    }

    public string? GetJsonRequestObjectString()
    {
        return NewJson;
    }

    public int GetSeed()
    {
        return Seed;
    }

    public List<FileAttachment> GetFileAttachments()
    {
        return attachments;
    }

    public async Task PostTxt2ImgRequest()
    {
        Tools.Tools tools = new();
        string? jsonTxtToImageRequestString = NewJson;

        string responseString = "";

        string host = _config["SDHostBasePrimary"];
        if (!tools.CheckHttpHostOnline(_config["SDHostBasePrimary"]).Result)
            host = _config["SDHostBaseSecondary"];

        using (var httpClient = new HttpClient { BaseAddress = new Uri(host) })
        {
            httpClient.Timeout = TimeSpan.FromMinutes(30);
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue("MMCSBot", "1.0"));

            try
            {
                var requestContent = new StringContent(jsonTxtToImageRequestString, Encoding.UTF8, "application/json");

                Console.WriteLine("posting content");
                var result = await httpClient.PostAsync("api/predict", requestContent);

                responseString = await result.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Request failed. {ex.Message}");
                throw new Exception("Request failed.");
            }
        }

        var jsonResponse = JsonNode.Parse(responseString);

        for (int i = 0; i < BatchSize; i++)
        {
            string base64ImageText = "";
            try
            {
                if (BatchSize == 1)
                    base64ImageText = jsonResponse?["data"]?[0][0]?.ToString();


                if (BatchSize > 1)
                {
                    base64ImageText = jsonResponse?["data"]?[0][i + 1]?.ToString();
                }
            }
            catch (Exception ex) { Console.WriteLine($"Warning: Probably ran out of VRAM. {ex}"); }


            if (base64ImageText != null)
            {
                //clean up browser/json base64
                base64ImageText = Regex.Unescape(base64ImageText);
                base64ImageText = base64ImageText?
                    .Remove(0, 22)
                    .Trim();
            }

            Stream stream = new MemoryStream(Convert.FromBase64String(base64ImageText));

            attachments?.Add(new FileAttachment(stream, $"{Guid.ToString()}-{i}.png", Prompt, false));
        }
    }
}