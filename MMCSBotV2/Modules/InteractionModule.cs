﻿using Discord.Interactions;
using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Text.Json.Nodes;
using System.Text.RegularExpressions;
using Discord.WebSocket;
using System.Net.Mail;
using System.Net;
using System.Security.Cryptography;
using System.Text.Json;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using MMCSBotV2.Tools;

namespace MMCSBotV2.Modules;

public class InteractionModule : InteractionModuleBase<SocketInteractionContext>
{

    public enum Orientation
    {
        Square,
        Landscape,
        Portrait
    }

    public enum BatchSize
    {
        One,
        Two,
        Three,
        Four,
        Five
    }

    public enum EnhanceType
    {
        None,
        Auto,
        Photo,
        Kemono,
        NSFW
    }
}
