﻿namespace MMCSBotV2.Tools
{
    public interface ITools
    {
        string CleanInput(string strIn);
        string SanitizeUrl(string input);
    }
}