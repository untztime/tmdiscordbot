﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MMCSBotV2.Modules;
using static MMCSBotV2.Modules.InteractionModule;

namespace MMCSBotV2.Tools;

internal class PromptEnhance
{
    private readonly IConfiguration _config;
    public PromptEnhance(IServiceProvider provider)
    {
        _config = provider.GetRequiredService<IConfigurationRoot>();
    }

    public string? InitialPrompt { get; set; }
    public string? InitialNegativePrompt { get; set; }
    public string? NewPrompt { get; set; }
    public string? NewNegativePrompt { get; set; }
    public EnhanceType Type { get; set; }

    public void GenerateEnhancedPrompt()
    {
        // data from config.json
        if (Type == EnhanceType.None)
        {
            NewPrompt = InitialPrompt;
            NewNegativePrompt = InitialNegativePrompt;
            return;
        }

        if (Type == EnhanceType.Auto)
        {
            NewPrompt = InitialPrompt + _config.GetSection("autoEnhancements").GetSection("Auto").GetSection("prompt").Value;
            NewNegativePrompt = InitialNegativePrompt + _config.GetSection("autoEnhancements").GetSection("Auto").GetSection("negativePrompt").Value;
            return;
        }

        if (Type == EnhanceType.Photo)
        {
            NewPrompt = InitialPrompt + _config.GetSection("autoEnhancements").GetSection("Photo").GetSection("prompt").Value;
            NewNegativePrompt = InitialNegativePrompt + _config.GetSection("autoEnhancements").GetSection("Photo").GetSection("negativePrompt").Value;
            return;
        }

        if (Type == EnhanceType.Kemono)
        {
            NewPrompt = InitialPrompt + _config.GetSection("autoEnhancements").GetSection("Kemono").GetSection("prompt").Value;
            NewNegativePrompt = InitialNegativePrompt + _config.GetSection("autoEnhancements").GetSection("Kemono").GetSection("negativePrompt").Value;
            return;
        }

        if (Type == EnhanceType.NSFW)
        {
            NewPrompt = InitialPrompt + _config.GetSection("autoEnhancements").GetSection("NSFW").GetSection("prompt").Value;
            NewNegativePrompt = InitialNegativePrompt + _config.GetSection("autoEnhancements").GetSection("NSFW").GetSection("negativePrompt").Value;
            return;
        }

    }
}
