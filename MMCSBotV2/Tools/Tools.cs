﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MMCSBotV2.Modules;

namespace MMCSBotV2.Tools;

public class Tools : ITools
{
    public string CleanInput(string strIn)
    {
        try
        {
            return Regex.Replace(strIn, @"[^\w\.@-]", " ", RegexOptions.None, TimeSpan.FromSeconds(1.5));
        }
        catch (RegexMatchTimeoutException) { return string.Empty; }
    }

    public async Task<string> ConvertUrlImageToBase64StringAsync(string url)
    {
        byte[] imageByteArray = null;
        using (var client = new HttpClient())
        {
            var response = client.GetAsync(url).GetAwaiter().GetResult();
            if (response.IsSuccessStatusCode)
            {
                imageByteArray = await response.Content.ReadAsByteArrayAsync();
            }
        }

        var imageBase64 = Convert.ToBase64String(imageByteArray);

        return imageBase64;
    }

    public async Task<bool> CheckHttpHostOnline(string baseUrl)
    {
        using (var client = new HttpClient())
        {
            var response = client.GetAsync(baseUrl).GetAwaiter().GetResult();
            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }


    public string SanitizeUrl(string input)
    {
        List<string> lstBad = new List<string>(new string[] { "\"", " ", "'" });
        string pattern = "";
        foreach (string badWord in lstBad)
            pattern += pattern.Length == 0 ? $"{badWord}" : $"|{badWord}";

        pattern = $@"\b({pattern})\b";

        return Regex.Replace(input, pattern, "", RegexOptions.IgnoreCase);
    }
}
